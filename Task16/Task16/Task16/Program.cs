﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Task16
{
    class Program
    {
        static void Main(string[] args)
        {
            {
                Console.WriteLine("Say Something:");
                string line = Console.ReadLine();
                Console.Write("You said ( ");
                Console.Write(line);
                Console.Write(" ) that contains ");
                Console.Write(line.Length);
                Console.WriteLine(" character(s)");
            }
        }
    }
}
