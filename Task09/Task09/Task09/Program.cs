﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Task09
{
    class Program
    {
        static void Main(string[] args)
        {
            int y = 0;
            for (y = 2017; y <= 2036; y++)
            {
                if (DateTime.IsLeapYear(y))
                {
                    Console.WriteLine("The year {0} is a leap year", y);
                }
            }
        }
    }
}
