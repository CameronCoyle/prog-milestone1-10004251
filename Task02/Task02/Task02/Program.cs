﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Task02
{
    class Program
    {
        static void Main(string[] args)
        {

            var date = 0;
            Console.WriteLine("What month were you born?");
            var month = (Console.ReadLine());
            Console.WriteLine("What date were you born, 1-31?");
            date = int.Parse(Console.ReadLine());

            Console.WriteLine($"Your month and date: {month}, {date} ");
        }
    }
}
