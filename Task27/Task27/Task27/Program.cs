﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Task27
{
    class Program
    {
        static void Main(string[] args)
        {
            List<string> Colours = new List<string>()
            {
        "Red",
        "Blue",
        "Yellow",
        "Green",
        "Pink"
    };
            Colours.Sort();
            foreach (string s in Colours)
            {
                Console.WriteLine(s);
            }
        }
    }
}