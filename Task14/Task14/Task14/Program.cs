﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Task14
{
    class Program
    {
        static void Main(string[] args)
        {
            Console.WriteLine("Enter a TB Value");
                var Input = double.Parse(Console.ReadLine());
                var Output = System.Math.Round(Input * 1024);
                Console.WriteLine($"{Input}TB is {Output}GB");
            }
        }
    }