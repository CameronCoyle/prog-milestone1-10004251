﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Task39
{
    class Program
    {
        static void Main(string[] args)
        {
            Console.WriteLine("Enter number of days in month");
            int days = Convert.ToInt32(Console.ReadLine());

            if (days >= 29)
            {
                Console.WriteLine("There are 5 Mondays in this month");
            }

            else
            {
                Console.WriteLine("There are 4 Mondays in this month");
                    }
        }
    }
}