﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Task32
{
    class Program
    {
        static void Main(string[] args)
        {
            string[] days = new string[7] {
                "Monday_Day_",
                "Tuesday_Day_",
                "Wednesday_Day_",
                "Thursday_Day_",
                "Friday_Day_",
                "Saturday_Day_",
                "Sunday_Day_" };

            int i = 1;

            foreach (string day in days)
            {
                Console.WriteLine($"{day} {i++}");
            }
        }
    }
}