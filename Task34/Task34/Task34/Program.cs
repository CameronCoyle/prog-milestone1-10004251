﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Task34
{
    class Program
    {
        static void Main(string[] args)
        {
            Console.WriteLine("Enter number of weeks");
            var output = Console.ReadLine();

            Console.Write("There are ");
            Console.Write( Convert.ToInt32(output) * 5);
            Console.Write(" working days in a week ");
        }
    }
}
