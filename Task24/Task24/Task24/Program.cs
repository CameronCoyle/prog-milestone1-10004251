﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Task24
{
    class Program
    {
        static void Main(string[] args)
        {
            Start:
            Console.WriteLine("*****************");
            Console.WriteLine("1. Press 1");
            Console.WriteLine("2. Press 2");
            Console.WriteLine("*****************");
            var Outcome = Console.ReadLine();

            {
                    Console.Clear();
                    Console.Write("Press m to go to start ");

                if (Console.ReadLine().Equals("m"))
                    goto Start;

                else
                    Console.Clear();
            }

        }
    }
}
