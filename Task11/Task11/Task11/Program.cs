﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Task11
{
    class Program
    {
        static void Main(string[] args)
        {
            Console.Write("Enter any year:");
            int y = int.Parse(Console.ReadLine());
            if ((y % 4 == 0) && (y % 100 != 0) || (y % 400 == 0))
            {
                Console.WriteLine("The year {0}, is a Leap Year", y);
            }
            else
            {
                Console.WriteLine("The year {0}, is not a Leap Year", y);
            }
        }
    }
}