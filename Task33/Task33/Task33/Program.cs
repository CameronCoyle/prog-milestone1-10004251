﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Task33
{
    class Program
    {
        static void Main(string[] args)
        {
            int People;
            int NumberOfGroups = 0;
            Console.WriteLine($"Enter number of people for study groups: ");

            People = int.Parse(Console.ReadLine());

            NumberOfGroups = (int)Math.Ceiling((double)People / (double)28);

            Console.WriteLine($"You will require {NumberOfGroups} Groups for {People} people");
        }
    }
}