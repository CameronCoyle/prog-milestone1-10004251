﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Task31
{
    class Program
    {
        static void Main(string[] args)
        {
            Start:
            Console.WriteLine("Enter a number to check if it is divisable by 3 or 4");
            double Input = Convert.ToDouble(Console.ReadLine());

            if (Input % 3 == 0)
            {
                Console.WriteLine(Input / 3);
                goto Start;
            }
            else if (Input % 4 == 0)
            {
                Console.WriteLine(Input / 4);
                goto Start;
            }
            else
            {
                Console.WriteLine("No");
                goto Start;
            }
        }
    }
}
