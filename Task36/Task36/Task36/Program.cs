﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Task36
{
    class Program
    {
        static void Main(string[] args)
        {
            var counter = 5;
            var i = 2;

            for (i = 0; i < counter; i++)
            {
                var a = i + 1;
                if (a == 2)
                    Console.WriteLine("Equal");

                else
                    Console.WriteLine("Not equal");
            }

        }
    }
}
