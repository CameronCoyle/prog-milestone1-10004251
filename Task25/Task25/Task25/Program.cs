﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Task25
{
    class Program
    {
        static void Main(string[] args)
        {
            Console.WriteLine("Enter the word hi or the number 1");
            string days = Console.ReadLine();

            switch (days)
            {
                case "1":
                    {
                        Console.WriteLine("Correct");
                        break;
                    }
                case "hi":
                    {
                        Console.WriteLine("Incorrect");
                        break;
                    }
                default:
                    {
                        Console.WriteLine("Start again and please enter a valid input");
                        break;
                    }
            }

        }
    }
}