﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Task06
{
    class Program
    {
        static void Main(string[] args)
        {
            int n = 0;

            for (int input = 0; input < 5; input++)
            {
                Console.Write("Please say a number");

                n = n + int.Parse(Console.ReadLine());
            }
            Console.WriteLine($"The total of numbers given = {n}");
        }
    }
}